# Sport Marks Swift

[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Tested using **Xcode 14.1**

Using: **Swift 5.7**

Architecture: **MVVMO** (MVVM + Observable pattern)

UI: **SwiftUI**

iOS deployment target: **16.0**

App uses my own [**MKUseful swift package**](https://gitlab.com/examples63/mkuseful)

**TODO:** Add laps to stopwatch feature
    
## Example task description

one list and one detail
Take future growth into consideration, e.g. multiple item types in table/collection View

• UIKit and Swift

### Example task (in Slovak)

https://jsonplaceholder.typicode.com/

Cieľom zadania je vytvoriť jednoduchú aplikáciu Galéria, ktorá umožňuje prehliadať obrázky na vzdialenom úložisku. Aplikácia má dve obrazovky:

• Náhľad všetkých obrázkov - vo forme listu alebo gridu 
• Detail obrázku

### Example task  Part 2 ( in Czech ) 

dvě obrazovky. První pro zadávání sportovních výkonů. Druhá bude sloužit pro výpis již zadaných sportovních výkonů.

Ukládat je na backend (např. Firebase, vlastní řešení, )  +  do lokální databáze (např. CoreData, Realm, ...) dle volby uživatele.
(
Docker containers in an ECS cluster
Amazon, Google, Digital Ocean, IBM cloud
Vapor has great Docker integration
) 
 

Nebude používat Storyboardy ani XIBy. Aplikace musí správně fungovat jak v landscape, tak v portrait módu. 
 
 
  Zadání názvu sportovního výkonu :
 
● Zadání místa konání sportovnímu výkonu
● Zadání délky trvání sportovního výkonu
● Výběr, zda položku uložit do lokální databáze nebo na backend
● Uložení položky sportovního výkonu do vybraného úložiště


  Výpis již zadaných sportovních výkonů :

● Výpis vložených položek dle výběru (All | Local | Remote)
● Barevně odlišené položky dle typu úložiště
