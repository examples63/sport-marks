//
//  SMAppSettings.swift
//  Sport Marks
//
//  Created by Martin Kompan on 06/12/2022.
//

import Foundation

struct SMAppSettings {
    static let dbName = "Sport_Marks"
}
