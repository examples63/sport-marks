//
//  Sport_MarksApp.swift
//  Sport Marks
//
//  Created by Martin Kompan on 15/10/2022.
//

import SwiftUI

@main
struct Sport_MarksApp: App {
    
    // MARK: Stored Properties
    
    let persistenceController = PersistenceController.shared
    
    // MARK: Scenes
    
    var body: some Scene {
        WindowGroup {
            MarksListView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
