//
//  MarkDetailView.swift
//  Sport Marks
//
//  Created by Martin Kompan on 12/12/2022.
//

import SwiftUI
import MKUseful

struct MarkDetailView: View {
    
    // MARK: Stored Properties
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @StateObject private var viewModel = ViewModel()
    
    let model:Mark
    
    // MARK: Views
    
    var body: some View {
        VStack{
            Text(viewModel.date).font(.system(size: 14)).fontWeight(.light).padding(5)
            HStack {
                Text(viewModel.name).font(.system(size: 22)).fontWeight(.bold).multilineTextAlignment(.leading)
                Spacer()
            }
            HStack {
                Text(viewModel.type).font(.system(size: 13)).fontWeight(.light).multilineTextAlignment(.leading).padding(5)
                Spacer()
            }
            HStack {
                Text(viewModel.value).font(.system(size: 22)).fontWeight(.bold).multilineTextAlignment(.leading)
                Spacer()
            }
            
            Spacer()
        }
        .padding(18)
        .onAppear(perform: { viewModel.update(model: model) } )
    }
}
