//
//  MarkDetailViewModel.swift
//  Sport Marks
//
//  Created by Martin Kompan on 12/12/2022.
//

import Foundation


extension MarkDetailView {
    @MainActor class ViewModel: ObservableObject {
        
        // MARK: Stored Properties
        
        private var model: Mark? {
            didSet {
                self.name = model?.name ?? ""
                
                self.date = model?.date?.stringWithFormat("dd MM YYYY hh:mm") ?? ""
                
                self.value = model?.stringFromValue() ?? ""
                
                self.type = "\(model?.type ?? "") :"
            }
        }
        
        @Published private(set) var name = String()
        
        @Published private(set) var date = String()
        
        @Published private(set) var value = String()
        
        @Published private(set) var type = String()
        
        // MARK: Class Methods
        
        func update(model: Mark) {
            self.model = model
        }
    }
}
