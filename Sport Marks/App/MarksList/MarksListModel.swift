//
//  MarksListModel.swift
//  Sport Marks
//
//  Created by Martin Kompan on 12/12/2022.
//

import CoreData
import SwiftUI
import MKUseful

class MarksListModel: NSObject, NSFetchedResultsControllerDelegate {
    
    // MARK: Stored Properties
    
    private var markFRC = NSFetchedResultsController<Mark>()
    
    private var listCallback: (([Mark]) -> Void)?
    
    private var moc: NSManagedObjectContext?
    
    let navigationBarTitle = "Marks"
    
    let trashImageName = "trash"
    
    let clearButtonTitle = "Clear"
    
    let addImageName = "plus"
    
    let addButtonTitle = "Add"
    
    let deleteAlertTitle = "Delete all Marks ?"
    
    let deleteAlertCancelButtonTitle = "Cancel"
    
    let deleteAlertConfirmButtonTitle = "DELETE"
    
    // MARK: Class Methods
    
    func initFRC(context moc: NSManagedObjectContext, marksCallback: @escaping ([Mark]) -> Void) {
        listCallback = marksCallback
        self.moc = moc
        
        let fetchRequest: NSFetchRequest<Mark> = Mark.fetchRequest()
        fetchRequest.sortDescriptors = []
        
        markFRC = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        markFRC.delegate = self
        self.fetchMarks()
    }
    
    func fetchMarks() {
        try? markFRC.performFetch()
        let marksList = markFRC.fetchedObjects ?? []
        listCallback?(marksList)
    }
    
    func deleteMarks(){
        try? self.moc?.deleteAll(Mark.self)
        try? self.moc?.save()
        listCallback?([])
    }
    
    // MARK: Delegate methods
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == markFRC {
            let marksList = markFRC.fetchedObjects ?? []
            listCallback?(marksList)
        }
    }
}
