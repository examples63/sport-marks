//
//  MarksListCellView.swift
//  Sport Marks
//
//  Created by Martin Kompan on 25/12/2022.
//

import SwiftUI

struct MarksListCellView: View {
    
    // MARK: Stored Properties
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @StateObject private var viewModel = MarkDetailView.ViewModel()
    
    let model:Mark
    
    // MARK: Views
    
    var body: some View {
        HStack(){
            VStack(){
                HStack{
                    Text(viewModel.name).font(.system(size: 22)).fontWeight(.bold)
                    Spacer()
                }
                HStack{
                    Text(viewModel.date).font(.system(size: 14)).fontWeight(.light)
                    Spacer()
                }
            }
            Spacer()
            VStack(){
                Text(viewModel.value).font(.system(size: 22)).fontWeight(.bold)
                Text(viewModel.type).font(.system(size: 13)).fontWeight(.light)
            }
        }
        .onAppear(perform: { viewModel.update(model: model) } )
    }
}
