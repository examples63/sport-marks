//
//  MarksListViewModel.swift
//  Sport Marks
//
//  Created by Martin Kompan on 12/12/2022.
//

import SwiftUI
import CoreData

extension MarksListView {
    
    @MainActor class ViewModel: ObservableObject {
        
        // MARK: Stored Properties
        
        @Published private(set) var marksList = [Mark]()
        
        @Published private(set) var navigationBarTitle = ""
        
        @Published private(set) var trashImageName = ""
        
        @Published private(set) var clearButtonTitle = ""
        
        @Published private(set) var addImageName = ""
        
        @Published private(set) var addButtonTitle = ""
        
        @Published private(set) var deleteAlertTitle = ""
        
        @Published private(set) var deleteAlertCancelButtonTitle = ""
        
        @Published private(set) var deleteAlertConfirmButtonTitle = ""
        
        private var model = MarksListModel()
        
        // MARK: Class Methods
        
        func update() {
            self.navigationBarTitle = model.navigationBarTitle
            self.trashImageName = model.trashImageName
            self.clearButtonTitle = model.clearButtonTitle
            self.addImageName = model.addImageName
            self.addButtonTitle = model.addButtonTitle
            self.deleteAlertTitle = model.deleteAlertTitle
            self.deleteAlertCancelButtonTitle = model.deleteAlertCancelButtonTitle
            self.deleteAlertConfirmButtonTitle = model.deleteAlertConfirmButtonTitle
        }
        
        func initFRC(context moc: NSManagedObjectContext) {
            model.initFRC(context: moc, marksCallback: { self.marksList = $0 })
        }
        
        func fetchMarks() {
            model.fetchMarks()
        }
        
        func clear(){
            model.deleteMarks()
        }
    }
}
