//
//  MarksListView.swift
//  Sport Marks
//
//  Created by Martin Kompan on 12/12/2022.
//

import SwiftUI
import MKUseful

fileprivate enum destination {
    case addNew
}

struct MarksListView: View {
    
    // MARK: Stored properties
    
    @StateObject private var viewModel = ViewModel()
    
    @Environment(\.managedObjectContext) var moc
    
    @State var path = NavigationPath()
    
    @State private var showingAlert = false
    
    // MARK: Views
    
    var body: some View {
        NavigationStack(path: $path) {
            List(viewModel.marksList){ mark in
                NavigationLink(value: mark) {
                    MarksListCellView(model: mark)
                }
            }
            .navigationBarTitle(viewModel.navigationBarTitle)
            .navigationDestination(for: Mark.self) { mark in
                MarkDetailView(model: mark)
            }
            .navigationDestination(for: destination.self) { destination in
                switch destination {
                case .addNew:
                    AddMarkView()
                }
            }
            .navigationBarItems(
                leading: Button(action: {
                    showingAlert = true
                }, label: {
                    HStack {
                        Image(systemName: viewModel.trashImageName).foregroundColor(Color.red)
                        Text(viewModel.clearButtonTitle).foregroundColor(Color.red)
                    }
                }),
                trailing: Button(action: {
                    path.append(destination.addNew)
                }, label: {
                    HStack {
                        Text(viewModel.addButtonTitle)
                        Image(systemName: viewModel.addImageName)
                    }
                })
            )
            .onAppear(perform: {
                viewModel.update()
                viewModel.initFRC(context: moc)
            })
        }
        .alert(viewModel.deleteAlertTitle, isPresented: $showingAlert) {
            Button(viewModel.deleteAlertCancelButtonTitle) { }
            Button(viewModel.deleteAlertConfirmButtonTitle) { viewModel.clear() }
        }
    }
}
