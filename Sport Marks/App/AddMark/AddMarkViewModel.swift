//
//  AddMarkViewModel.swift
//  Sport Marks
//
//  Created by Martin Kompan on 13/12/2022.
//

import Foundation
import CoreData

extension AddMarkView {
    
    @MainActor class ViewModel: ObservableObject {
        
        // MARK: Stored Properties
        
        private var model = AddMarkModel()
        
        @Published var type = ValueType.points
        
        @Published var name = ""
        
        @Published var points = "" {
            didSet {
                if points != "" {
                    addButtonDisabled = false
                } else {
                    addButtonDisabled = true
                }
            }
        }
        
        @Published private(set) var time = 0.0 {
            didSet{
                if time == 0.0 { addButtonDisabled = true }
                timeString = String(format: "%.1f", time)
            }
        }
        
        @Published private(set) var timeString = ""
        
        @Published var watchButtonTtile = ""
        
        @Published var timerRunning = false {
            didSet {
                if timerRunning {
                    timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
                    watchButtonTtile = model.watchButtonStop
                } else {
                    timer.invalidate()
                    watchButtonTtile = model.watchButtonStart
                    if (time > 0.0) {addButtonDisabled = false}
                }
            }
        }
        
        private var timer = Timer()
        
        @Published var addButtonDisabled = true
        
        // MARK: Static UI Properties
        
        @Published private(set) var watchButtonReset = ""
        
        @Published private(set) var addMarkButtonTitle = ""
        
        @Published private(set) var navigationBarTitle = ""
        
        @Published private(set) var nameTextFieldPlaceholder = ""
        
        @Published private(set) var pointsTextFieldPlaceholder = ""
        
        @Published private(set) var typePointsTitle = ""
        
        @Published private(set) var typeTimeTitle = ""
        
        @Published private(set) var valueTypeTitle = ""
        
        // MARK: Class Methods
        
        func update() {
            self.navigationBarTitle = model.navigationBarTitle
            self.addMarkButtonTitle = model.addMarkButtonTitle
            self.nameTextFieldPlaceholder = model.nameTextFieldPlaceholder
            self.typeTimeTitle = model.typeTimeTitle
            self.typePointsTitle = model.typePointsTitle
            self.pointsTextFieldPlaceholder = model.pointsTextFieldPlaceholder
            self.valueTypeTitle = model.valueTypeTitle
            self.watchButtonReset = model.watchButtonReset
            self.watchButtonTtile = model.watchButtonStart
        }
        
        func initFRC(context moc: NSManagedObjectContext) {
            model.initFRC(context: moc)
        }
        
        func addMark() {
            var value = 0.0
            
            switch (type){
            case .points:
                value = Double(points) ?? 0.0
            case .time:
                value = time
            }
            model.addMark(value: value, type: type.rawValue, name: self.name)
        }
        
        @objc func updateTime() {
            time += 0.1
        }
        
        func resetTimer(){
            timerRunning = false
            time = 0.0
        }
    }
}
