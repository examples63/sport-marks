//
//  AddMarkModel.swift
//  Sport Marks
//
//  Created by Martin Kompan on 13/12/2022.
//

import Foundation
import CoreData

class AddMarkModel {
    
    // MARK: Stored Properties
    
    private var moc: NSManagedObjectContext?
    
    let navigationBarTitle = "New Mark"
    let addMarkButtonTitle = "Add new Mark"
    let nameTextFieldPlaceholder = "Athlete's name"
    let pointsTextFieldPlaceholder = "Points"
    let typePointsTitle = "Points"
    let typeTimeTitle = "Time"
    let valueTypeTitle = "Value type: "
    let watchButtonStart = "Start"
    let watchButtonStop = "Stop"
    let watchButtonReset = "Reset"
    
    // MARK: Class Methods
    
    func initFRC(context moc: NSManagedObjectContext) {
        self.moc = moc
    }
    
    func addMark(value: Double, type: String, name: String) {
        if let moc = self.moc {
            let new = Mark(context: moc)
            new.value = value
            new.name = name
            new.date = Date()
            new.type = type
            try? moc.save()
        } else {
            NSLog("Error: ManagedObjectContext not found! (AddMarkModel)")
        }
    }
}
