//
//  AddMarkView.swift
//  Sport Marks
//
//  Created by Martin Kompan on 13/12/2022.
//

import SwiftUI

struct AddMarkView: View {
    
    // MARK: Stored properties
    
    @Environment(\.managedObjectContext) var moc
    
    @Environment(\.dismiss) var dismiss
    
    @StateObject private var viewModel = ViewModel()
    
    // MARK: View
    
    var body: some View {
        VStack{
            Picker(viewModel.valueTypeTitle, selection: $viewModel.type) {
                Text(viewModel.typePointsTitle).tag(ValueType.points)
                Text(viewModel.typeTimeTitle).tag(ValueType.time)
            }
            .pickerStyle(.segmented)
            .padding(12)
            TextField(viewModel.nameTextFieldPlaceholder, text: $viewModel.name)
                .padding(18)
            if viewModel.type == ValueType.points {
                TextField(viewModel.pointsTextFieldPlaceholder, text: $viewModel.points)
                    .keyboardType(.numberPad)
                    .padding(18)
            } else if (viewModel.type == ValueType.time) {
                Text(viewModel.timeString).font(.largeTitle).fontWeight(.bold).padding(12)
                HStack{
                    Button(viewModel.watchButtonReset){
                        viewModel.resetTimer()
                    }.padding(30)
                    Spacer()
                    Button(viewModel.watchButtonTtile) {
                        if viewModel.timerRunning {
                            viewModel.timerRunning = false
                        } else {
                            viewModel.timerRunning = true
                        }
                    }.padding(30)
                }
            }
            Button(viewModel.addMarkButtonTitle) {
                viewModel.addMark()
                dismiss()
            }.disabled(viewModel.addButtonDisabled)
                .padding(12)
                .navigationBarTitle(viewModel.navigationBarTitle)
                .onAppear(perform: {
                    viewModel.update()
                    viewModel.initFRC(context: moc)
                })
            Spacer()
        }
        
    }
}
