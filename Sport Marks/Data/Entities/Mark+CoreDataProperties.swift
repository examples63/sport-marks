//
//  Mark+CoreDataProperties.swift
//  Sport Marks
//
//  Created by Martin Kompan on 16/12/2022.
//
//

import Foundation
import CoreData

enum ValueType: String {
    case points = "points"
    case time = "time"
}

extension Mark {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mark> {
        return NSFetchRequest<Mark>(entityName: "Mark")
    }
    
    @NSManaged public var date: Date?
    @NSManaged public var value: Double
    @NSManaged public var type: String?
    @NSManaged public var name: String?
    
    func stringFromValue() -> String {
        if let type = ValueType(rawValue: self.type ?? ""){
            switch type {
            case .points:
                return String(format: "%.0f", self.value)
            case .time:
                return String(format: "%.2f sec.", self.value)
            }
        }
        return ""
    }
}
